# miniproject-scientific-computing2022

My name is Mohamad Harrouk.

I am interested in simulation in fluid mechanics and I hope to be a researcher in this domain.
Python Language is a powerful tool that serves researchers to perform their projects.
I aim from my mini-project to learn my first steps in peforming simple flow simulations by using Python.

# Project Description

Title: Navier-Stokes Equations Simplifier (NSES)
A program that simplifies Navier-Stokes equations by defining the flow conditions.
Also, the program will be able to perform simple numerical computation for the simplified NS equations.
The code is performed by using jupyter lab (also jupyter notebook can be used).

# Purpose:

The main goal of NSES program is to be able to simplify the local form of Navier-Stokes equations and discreate them into simpler form.

# Plan

My mini-project will include three main parts:

Part 1 : In this part I am going to define the local Navier-Stokes equations and to define different considerations. The user will be able to enter the flow conditions included in a certain problem.
The program will simplify the loacal N-S equations according to the considerations chosen by the user.
    
    
Part 2 : In this part the user will be able to define the involved fluid in his problem and flow properties (Density of his fluid and viscosity)
Also one main goal of this part is to define the known boundary conditions and acting forces.
    
    
Part 3 : In this part the user will be able to make simple numerical computation for the simplified 1D flow in purpose to estimate the missing variable(such as pressure or velocity)
For future goals, I aim to develop this program to solve 2D flows to find the velocity profile.
Also, in this part the user is able to represent the distrubution of pressure and velocity with matplotlib.\
The most amazing feature in this part is the capability of the program to **plot the contour of velocity and pressure distribution**, this achievment was my main goal from this miniproject: To start my first steps in flow simulation.

    
# Main Flaws:

The main flaw of this program is that the code on jupyter notebook or jupyter lab have to be run cell by cell and the user have to be aware before runing each cell, 
because any missing input will enforce the user to repeat runing the code from the begining!

Another flaw, in the third and fourth task (flow dimensions and uniform flow selection) I used many lines to perform the code, 
after completing the code I recognized that there is easier way to write these part by using sympy.subs function and replacing the neglected terms by zero.
However I keep the previous form to avoid mistakes.


Unfortunately I was not able to use Voila to control the widgets in the program.

# Libraries:

1. Sympy: To develop the local Navier-Stokes equation, and to make the numerical calculation of the simplified form.
in addition to the ability of this library to transform the input letters to mathematical symbols and making equations;
also this library is able to do intergrations and mathematical calculations, this feature is used in last part where numerical calculation is performed.

2. ipyWidgets: This library allow the user to enter some inputs by using widgets, such as: Checkbox, Text, Sliders, dropdowns...etc.
These inputs can be used to make the program able to do some actions. In our case, the user can define flow conditions by different used widgets and the program will simplify the Local NS equations regarding these inputs.

3. Pandas: I used pandas in purpose to insert a table that contains the properties of each fluid (escpecially the density and viscosity). The user can choose the fluid involved in his problem
and from the inserted table, the flow properties will be chosen by the program.

4. Numpy and MatplotLib: Both Libraries are used at the end to the notebook to arrange the result in arrays and to plot the distribution of velocity or pressure over the selected displacement range.











